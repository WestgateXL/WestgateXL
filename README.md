# WestgateXL

[![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)](#) [![forthebadge](https://forthebadge.com/images/badges/contains-cat-gifs.svg)](#) [![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)](#)


<p align="center">
    <img width="800" height="auto" src="http://download2266.mediafire.com/907o7wdn5ifg/x662xh0jggttj72/instances.png" alt="WestgateXL" />
</p>

<details>
 <summary><strong>Table of Contents</strong> (click to expand)</summary>

- [Getting Started](#-getting-started)
- [Features](#️-features-working-on)
- [Compilation](#️-compilation)
- [Compilation Requirements](#-requirements)
- [Compilation Steps](#-steps)
- [Packaging](#-packaging)
- [Built and Managed with](#️-built-and-managed-with)
- [Contributing](#-contributing)
- [Versioning](#-versioning)
- [Authors](#-authors)
- [License](#-license)
- [Todos](#-todos)
  </details>

## 🚀 Getting Started

Below you will find everything you need to know about the launcher. If you want to download a stable release you can do it from our official website (https://gitlab.com/WestgateXL/WestgateXL/-/packages). 

## 🎮 Download

To download the latest version, you can either click [here](https://gitlab.com/WestgateXL/WestgateXL/-/packages) and select the appropriate version for your operating system, or visit our [website](https://gitlab.com/WestgateXL/westgateui/wikis/How-We-Use-Versions).

## 🎨 Features (Working on)

- [x] Java Autorunner. You don't need to have java installed, a suitable version will be downloaded automatically.
- [ ] Log console. Always know what's happening.
- [x] Easy installation. It's as easy as pie to install both the vanilla game and forge. No further action from the user is required.
- [ ] Built-in autoupdater. You will never need to download a new version manually.
- [ ] Vanilla, Forge, and Curse modpacks download and autoupdater.
- [ ] Built-in manager for Minecraft servers.
- [ ] Cloud sync of game saves. You will never lose your saves again!

You can also:

- [ ] Import and export modpacks from and to other launchers
- [ ] Drag and drop instances wherever you like them, just like in your desktop
- [x] Keep track of the time you played each instance
- [ ] Add instances to the download queue, they will automatically download one after the other
- [ ] Manage your minecraft skin directly from the launcher
- [ ] Directly connect to a server from the launcher using quick launch

Keep in mind that not all of these features are yet part of the launcher. We are constantly updating the code adding new features. Feel free to help us :)



## 🚀 Built and managed with

- [Javascript](https://developer.mozilla.org/bm/docs/Web/JavaScript) - Language used
- [React](https://reactjs.org/) - JS Framework
- [Redux](https://redux.js.org/) - React state management
- [NodeJS](https://nodejs.org/en/) - JS Runtime
- [Electron](https://electronjs.org/) - JS Framework
- [Travis CI](https://travis-ci.org/) - CI Service
- [Codacy](https://www.codacy.com/) - Automated code review
- [Webpack](https://webpack.js.org/) - JS module bundler
- [Babel](https://babeljs.io/) - JS Transpiler
- [ESLint](https://eslint.org/) - JS Linter
- [Ant Design](https://ant.design/) - UI Design Language

## Developers 👨‍💻 
 
 ⚙️ Requirements
You need the following softwares installed:

Nodejs (> 8)
yarn
▶️ Steps
Install the dependencies and devDependencies.

1. $ `cd WestgateXL`
2. $ `yarn`


### Start the environments one or the other

3. $ `yarn dev`
For production environment...
         or
3. $ `yarn start`

💾 Compilation
These are the steps to compile it yourself. Be sure to follow instructions 1-3, then continue below

🚚 Packaging
To package apps for the local platform:

`If you want to disable it in develop to show up by default just add **devTools: false** after **experimentalFeatures** main.dev.js` 

$ yarn package
To package apps for all platforms:

First, refer to Multi Platform Build for dependencies.

Then,

$ yarn package-all




## ❤️ Authors

- **Davide Ceschia** - _Help & Support_ - [GorillaDevs](https://github.com/gorilla-devs) or [Email the Gorillas](mailto:info@gdevs.io) Thanks bud!

`See also the list of [contributors](https://gitlab.com/WestgateXL/WestgateXL/graphs/master) who participated in this project.`

## 🎁  Contributing

1.  Fork it! (if you see a branch beginning with **p**-BRANCHNAME, use that branch, else you will have to wait until it becomes available
2.  Create your feature branch:  `git checkout -b my-new-feature`
3.  Commit your changes:  `git commit -am 'Add some feature'`
4.  Push to the branch:  `git push origin my-new-feature`
5.  Submit a pull request :D


## 🎓 License

This project is licensed under the GNU GPL V3.0 - see the [LICENSE](LICENSE) file for details

## ✏️ Todos

Here is the complete list of things we want to do. If you want to help us doing them or want to suggest some new ideas, comment here!
[TODOS/IDEAS](https://gitlab.com/WestgateXL/WestgateXL/boards)